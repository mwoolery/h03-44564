# 44-564 H03 Working with Python

Basic python introduction and concepts needed 
in preparation for working with MapReduce

# Requirements

- Install Chocolatey, the Windows package manager.
- Install Python 2.6.6 
- Install Git for Windows
- Install TortoiseGit
- Add Open Command Window Here as Administrator to your File Explorer context menu.
- Install Notepad++ or 
- Visual Studio Code for text editing (w/Python extension by Microsoft >5M istalls)

# References

See Udacity "Introduction to Hadoop and MapReduce"
- https://classroom.udacity.com/courses/ud617/